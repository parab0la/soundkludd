package com.soundkludd.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class CredentialModel implements Serializable {

    private String eMail;
    private String permaLink;
    private String permaLinkUrl;

    public CredentialModel() {
    }

    public CredentialModel(String eMail, String permaLink, String permaLinkUrl) {
        this.eMail = eMail;
        this.permaLink = permaLink;
        this.permaLinkUrl = permaLinkUrl;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPermaLink() {
        return permaLink;
    }

    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }

    public String getPermaLinkUrl() {
        return permaLinkUrl;
    }

    public void setPermaLinkUrl(String permaLinkUrl) {
        this.permaLinkUrl = permaLinkUrl;
    }

    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}