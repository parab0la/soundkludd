package com.soundkludd.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel implements Serializable{

    @Id
    private Long id;
    private Long soundCloudId;
    @Embedded
    private NameModel nameModel;
    @Embedded
    private CredentialModel credentialModel;
    private String avatarUrl;
    private TrackModel trackModel;

    public UserModel() {
    }

    public UserModel(Long id, Long soundCloudId, NameModel nameModel, CredentialModel credentialModel, String avatarUrl, TrackModel trackModel) {
        this.id = id;
        this.soundCloudId = soundCloudId;
        this.nameModel = nameModel;
        this.credentialModel = credentialModel;
        this.avatarUrl = avatarUrl;
        this.trackModel = trackModel;
    }

    public Long getId() {
        return id;
    }

    public Long getSoundCloudId() {
        return soundCloudId;
    }

    public void setSoundCloudId(Long soundCloudId) {
        this.soundCloudId = soundCloudId;
    }

    public NameModel getNameModel() {
        return nameModel;
    }

    public void setNameModel(NameModel nameModel) {
        this.nameModel = nameModel;
    }

    public CredentialModel getCredentialModel() {
        return credentialModel;
    }

    public void setCredentialModel(CredentialModel credentialModel) {
        this.credentialModel = credentialModel;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public TrackModel getTrackModel() {
        return trackModel;
    }

    public void setTrackModel(TrackModel trackModel) {
        this.trackModel = trackModel;
    }

}