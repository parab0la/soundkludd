package com.soundkludd.services;


import com.soundkludd.models.UserModel;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserService {

    @PersistenceContext
    private EntityManager entityManager;

    public void saveUser (UserModel userModel){

        entityManager.persist(userModel);
    }
}
