package com.soundkludd.controllers;

import com.soundkludd.models.NameModel;
import com.soundkludd.models.UserModel;
import com.soundkludd.services.UserService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    @EJB
    private UserService userService;

    @GET
    @Path("test")
    // This works with end point http://localhost:8080/rest/user/test/
    public Response test() {
        UserModel userModel = new UserModel();
        userModel.setNameModel(new NameModel("Thomas", "Lansing", "Thompadude"));
        return Response.ok(userModel).build();
    }

    @POST
    @Path("save-user")
    // This should work with end point http://localhost:8080/rest/user/save-user/
    public Response saveUser(UserModel userModel) {
        if (userModel != null) {
            System.out.println("userModel is not null!");
        }

        if(userService == null) {
            System.out.println("userService är null!!!");
        }

        userService.saveUser(userModel);

        return Response.status(201).entity(userModel).build();
    }

}