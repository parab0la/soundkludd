SC.initialize({
    client_id: '37a4e59339e9ae7365f169738f568d72',
    redirect_uri: 'http://localhost:8080/callback.html'
});

SC.get('/users/209676908/tracks').then(function (tracks) {
    console.log('*LOG* tracks[0].title');
    console.log(tracks[0].title);
});

// Code below is ReactJS! :-)

var SCConnect = React.createClass({
    getInitialState: function () {
        return {
            "id": null,
            "kind": null,
            "permalink": null,
            "username": "",
            "last_modified": null,
            "uri": null,
            "permalink_url": null,
            "avatar_url": null,
            "country": null,
            "first_name": null,
            "last_name": null,
            "full_name": null,
            "description": null,
            "city": null,
            "discogs_name": null,
            "myspace_name": null,
            "website": null,
            "website_title": null,
            "online": false,
            "track_count": null,
            "playlist_count": null,
            "plan": null,
            "public_favorites_count": null,
            "subscriptions": null,
            "upload_seconds_left": null,
            "quota": {
                "unlimited_upload_quota": null,
                "upload_seconds_used": null,
                "upload_seconds_left": null
            },
            "private_tracks_count": null,
            "private_playlists_count": null,
            "primary_email_confirmed": null,
            "locale": null,
            "followers_count": null,
            "followings_count": null
        }
    },

    render: function () {
        return (
            <div>
                <div>Logged in: {this.props.data}</div>
                <button onClick={this.handleClick}>
                    SC.Connect()
                </button>
                <br/>
            </div>
        )
    },

    handleClick: function () {
        console.log('*LOG* this.state');
        console.log(this.state);
        console.log('*LOG* this.state.username');
        console.log(this.state.username);
        this.connectToSoundCloud();
    },

    connectToSoundCloud: function () {
        SC.connect().then(function () {
            return SC.get('/me');
        }).then(function (me) {
            console.log('*LOG* typeof me');
            console.log(typeof me);
            console.log('*LOG* me');
            console.log(me);
            // AJAX get call here. Need to use this state and store the currently logged in user.
            var loggedInUser = me;
            console.log('*LOG* loggedInUser:');
            console.log(loggedInUser);
            // this.setState({
            //     "id": me.id,
            //     "kind": me.kind,
            //     "permalink": me.permalink,
            //     "username": loggedInUser.username,
            //     "last_modified": me.last_modified,
            //     "uri": me.uri,
            //     "permalink_url": me.permalink_url,
            //     "avatar_url": me.avatar_url,
            //     "country": me.country,
            //     "first_name": me.first_name,
            //     "last_name": me.last_name,
            //     "full_name": me.full_name,
            //     "description": me.description,
            //     "city": me.city,
            //     "discogs_name": me.discogs_name,
            //     "myspace_name": me.myspace_name,
            //     "website": me.website,
            //     "website_title": me.website_title,
            //     "online": me.online,
            //     "track_count": me.track_count,
            //     "playlist_count": me.playlist_count,
            //     "plan": me.plan,
            //     "public_favorites_count": me.public_favorites_count,
            //     "subscriptions": me.subscriptions,
            //     "upload_seconds_left": me.upload_seconds_left,
            //     "quota": {
            //         "unlimited_upload_quota": me.quota.unlimited_upload_quota,
            //         "upload_seconds_used": me.quota.upload_seconds_used,
            //         "upload_seconds_left": me.quota.upload_seconds_left
            //     },
            //     "private_tracks_count": me.private_tracks_count,
            //     "private_playlists_count": me.private_playlists_count,
            //     "primary_email_confirmed": me.primary_email_confirmed,
            //     "locale": me.locale,
            //     "followers_count": me.followers_count,
            //     "followings_count": me.followings_count
            // }.bind(this));

            // This does not work.
            // that.setState({username: loggedInUser.username}.bind(this), function () {
            //     console.log('setState was called.');
            // });
            var userToPostObject = {
                "id": null,
                "soundCloudId": null,
                "nameModel": {
                    "firstName": me.first_name,
                    "lastName": me.last_name,
                    "userName": me.username
                },
                "credentialModel": {},
                "avatarUrl": null,
                "trackModel": null
            };
            var jsonObject = JSON.stringify(userToPostObject);
            $.post('http://localhost:8080/rest/user/save-user/', jsonObject);
        });
    }
});

var SCConnectResult = React.createClass({
    render: function () {
        return (
            <div>
                <button onClick={this.handleClick}>
                    Updates your SoundCloud profile description to "I am using the SoundCloud API!"
                </button>
            </div>
        )
    },
    handleClick: function () {
        SC.connect().then(function () {
            SC.put('/me', {
                user: {description: 'I am using the SoundCloud API!'}
            });
        });
    }
});

ReactDOM.render(
    <div>
        <SCConnect />
        <SCConnectResult/>
    </div>,
    document.getElementById('wrapper')
);

// Trying a AJAX call with ReactJS.

var Test = React.createClass({
    getInitialState: function () {
        return {
            "id": null,
            "soundCloudId": null,
            "nameModel": {
                "firstName": '',
                "lastName": '',
                "userName": ''
            },
            "credentialModel": null,
            "avatarUrl": null,
            "trackModel": null
        }
    },

    componentDidMount: function () {
        this.serverRequest = $.get(this.props.source, function (result) {
            console.log('*LOG* componentDidMount was executed.');
            // var lastTest = result[0];
            var lastTest = result;
            console.log('*LOG* var lastTest:');
            console.log(lastTest);
            console.log('*LOG* var lastTest.nameModel:');
            console.log(lastTest.nameModel);
            console.log('*LOG* typeof lastTest:');
            console.log(typeof lastTest);
            this.setState({
                "id": 1,
                "soundCloudId": null,
                "nameModel": {
                    "firstName": lastTest.nameModel.firstName,
                    "lastName": lastTest.nameModel.lastName,
                    "userName": lastTest.nameModel.userName
                },
                "credentialModel": null,
                "avatarUrl": null,
                "trackModel": null
            })
        }.bind(this));
    },

    componentWillUnmount: function () {
        console.log('*LOG* componentWillUnmount was executed.');
        this.serverRequest.abort();
    },

    render: function () {
        console.log('*LOG* this.state:');
        console.log(this.state);
        console.log('*LOG* this.state.nameModel.firstName:');
        console.log(this.state.nameModel.firstName);
        var userToView = this.state;
        console.log('*LOG* userToView:');
        console.log(userToView);
        console.log('*LOG* this.state.id');
        console.log(this.state.id);
        return (
            <div>
                Testing this.props.source: {this.props.source}
                <br/>
                Testing this.state.nameModel.firstName: {this.state.nameModel.firstName}
                <br/>
                Testing this.state.id: {this.state.id}
            </div>
        )
    }

});

ReactDOM.render(
    <Test source="http://localhost:8080/rest/user/test/"/>,
    document.getElementById('ajax-test')
);

// Below code is just some messing around with ReactJS for practice. Safe to delete!

var TestReact = React.createClass({
    render: function () {
        return (
            <div>{this.props.testProp}</div>
        )
    }
});

ReactDOM.render(
    <TestReact testProp='The Test Works!'/>,
    document.getElementById('prop-test')
);

// https://facebook.github.io/react/docs/tutorial.html
// http://jasonjl.me/blog/2015/04/18/rendering-list-of-elements-in-react-with-jsx/

var testData = [{id: 1, name: 'Test Person A'}, {id: 2, name: 'Test Person B'}];

var TestArrayProp = React.createClass({
    render: function () {
        return (
            <ul>
                {this.props.list.map(function (listValue) {
                    return <li key={listValue.id}>{listValue.name}</li>
                })}
            </ul>
        )
    }
});

ReactDOM.render(
    <TestArrayProp list={testData}/>,
    document.getElementById('prop-array-test')
);